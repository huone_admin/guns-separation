package cn.stylefeng.guns.core.pojo.login;

import lombok.Data;

import java.io.Serializable;

/**
 * @author ZhangZhao
 * @Description 登录用户VO类
 * @date 2021年08月17日 11:46
 */
@Data
public class  LoginUserVO implements Serializable {
    /**
     * 主键id
     */
    private Long id;
    /**
     * 账号
     */
    private String account;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 名字
     */
    private String name;
    /**
     * 移动电话（手机号）
     */
    private String phone;
    /**
     * 电话（座机）
     */
    private String tel;
    /**
     * 部门id
     */
    private Long deptId;
    /**
     * 部门名称
     */
    private String deptName;
    /**
     * 部门行政登记
     */
    private Integer grade;
    /**
     * 是否未绑定微信
     */
    private Boolean unBindWx;
}
